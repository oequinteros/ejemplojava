package ejemplojava;

public class Persona {
    String nombre;
    String apellido;
    Integer dni;
    public Persona(String nombre, Integer dni){
        this.nombre=nombre;
        this.dni=dni;
    }
    @Override
    public boolean equals(Object obj) {
        Persona p = (Persona)obj;
        return dni.equals(p.dni);
    }
}
